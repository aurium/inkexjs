#!/usr/bin/env coffee

inkex = require './inkex'

inkex.args
  .version('0.0.1')
  .option('-c, --color [value]', 'color to set on the selected ids')

inkex.filter (data)->
  for id in inkex.args.id
    [tag, content] = inkex.getElementById id, data
    style = inkex.parseStyle content.$.style
    style.fill = inkex.args.color
    content.$.style = inkex.stringfyStyle style

