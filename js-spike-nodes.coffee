#!/usr/bin/env coffee

inkex = require './inkex'

inkex.args
  .version('0.0.1')
  .option('-s, --size [value]', 'the spike size', parseInt)

inkex.filter (data)->
  size = inkex.args.size
  for id in inkex.args.id
    [tag, content] = inkex.getElementById id, data
    if tag isnt 'path'
      throw new Error 'You selected a non path element.'
    path = inkex.parsePath content.$.d
    newPath = [];
    for cmd in path
      newPath.push cmd
      if cmd.cmd not in ['z','Z']
        lastDot = cmd.pos[cmd.pos.length-1]
        newPath.push cmd:'l', pos:[{ x:-1*size, y:-1*size }]
        newPath.push cmd:'l', pos:[{ x: 2*size, y: 2*size }]
        newPath.push cmd:'l', pos:[{ x:-1*size, y:-1*size }]
    content.$.d = inkex.stringfyPath newPath

