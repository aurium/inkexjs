#!/usr/bin/env coffee

inkex = require './inkex'
fs = require 'fs'

inkex.input (file, xml)->
  data = fs.readFileSync(file).toString().split '\n'
  textList = xml.svg.text = []
  textList.push $:{y:0, style:'font-size:6px; fill:blue'}, _:'Text by JS'
  i=1
  for line in data
    textList.push $:{y:20*i++, 'xml:space':'preserve'}, _:line

