#!/usr/bin/env coffee

inkex = require './inkex'

inkex.args
  .version('0.0.1')
  .option('-c, --color [value]', 'color to set on the selected ids')
  .option('-r, --ray [value]', 'the circle ray', parseInt)
  .option('-x, --cx [value]', 'the circle\'s center horizontal position', parseInt)
  .option('-y, --cy [value]', 'the circle\'s center vertical position', parseInt)

inkex.filter (data)->
  data.svg.circle = [] unless data.svg.circle?
  data.svg.circle.push $: {
    r: inkex.args.ray
    cx: inkex.args.cx
    cy: inkex.args.cy
    fill: inkex.args.color
  }
