'use strict';

var fs = require('fs');
var missedModules = [];

try { var xml2js = require('xml2js'); }
catch(e) { missedModules.push('xml2js'); }
try { var args = require('commander'); }
catch(e) { missedModules.push('commander'); }

if ( missedModules.length > 0 ) {
  console.error(
    'You missed some modules to run this extension.\n' +
    'Try to install with this command:\n' +
    'sudo npm install -g ' + missedModules.join(' ')
  );
  process.exit(1);
}

args.option(
  '-i, --id [value]', 'refer to an SVG element',
  function(val, mem){ mem.push(val); return mem }, []
);

exports.args = args;

exports.getElementById = function getElementById(id, branch) {
  if (!branch || typeof branch !== 'object') return [null, null];
  for (var tag in branch) {
    var content = branch[tag];
    if (content.$ && content.$.id === id) return [tag, content];
    else {
      if (content instanceof Array) {
        for (var c,i=0; c=content[i]; i++) {
          if (c.$ && c.$.id === id) return [tag, c];
          else {
            var findEl = getElementById(id, c);
            if (findEl[0]) return findEl;
          }
        }
      } else if (tag !== '$') return getElementById(id, content);
    }
  }
  return [null, null];
};

exports.parseStyle = function(style) {
  var styleObj = {};
  style = (style||'').toString().split(';');
  for (var item,i=0; item=style[i]; i++) {
    item = item.split(':');
    if (item[1]) styleObj[item[0]] = item[1];
  }
  return styleObj;
};

exports.stringfyStyle = function(style) {
  var styleStr = '';
  for (var key in style) styleStr += key + ':' + style[key] + ';';
  return styleStr;
};

function posStr2Obj(pos) {
  pos = pos.split(',');
  return {
    x: parseFloat(pos[0]),
    y: parseFloat(pos[1])
  };
}

function appendCmd(pathObj, lastCmd, pathTmpList) {
  var pathCmd = pathTmpList[0];
  if (/^[a-z]$/i.test(pathCmd)) pathTmpList.shift();
  if (lastCmd === 'm') lastCmd = 'l';
  if (lastCmd === 'M') lastCmd = 'L';
  if (!/^[a-z]$/i.test(pathCmd)) pathCmd = lastCmd;
  if (!pathCmd) throw new Error('The path "' + pathTmpList.join(' ') + '" looks wrong...');
  var cmd = { cmd:pathCmd, pos:[] };
  switch (pathCmd) {
    case 'm':
    case 'M': cmd.pos.push(posStr2Obj(pathTmpList.shift())); break;
    case 'l':
    case 'L': cmd.pos.push(posStr2Obj(pathTmpList.shift())); break;
    case 'c':
    case 'C': for (var i=0; i<3; i++)
                cmd.pos.push(posStr2Obj(pathTmpList.shift()));
              break;
    case 'z':
    case 'Z': cmd.pos = null; break;
    default: throw new Error('Unknow path command ' + pathCmd + '.');
  }
  pathObj.push(cmd);
  return pathCmd;
}

exports.parsePath = function(pathStr) {
  var lastCmd = null;
  var pathObj = [];
  var pathTmpList = (pathStr||'').toString().replace(/^\s*|\s*$/g, '').split(/[\s]+/);
  while (pathTmpList.length > 0) lastCmd = appendCmd(pathObj, lastCmd, pathTmpList);
  return pathObj;
};

exports.stringfyPath = function(pathObj) {
  var cmd, p, pathTmp, pos, _i, _len;
  var pathTmp = [];
  for (var cmd,i=0; cmd=pathObj[i]; i++) {
    var pos = cmd.pos || [];
    for (var p,j=0; p=pos[j]; j++) pos[j] = p.x + ',' + p.y;
    pathTmp.push( cmd.cmd + ' ' + pos.join(' ') );
  }
  return pathTmp.join(' ');
};

exports.filter = function(fn) {
  args.parse(process.argv);
  var inputSVG = process.argv[process.argv.length-1]
  xml2js.parseString(fs.readFileSync(inputSVG), function(err, data) {
    if (err) console.error(err);
    else {
      try { fn(data); }
      catch (err) {
        console.error(
          err.message +'\n\n'+
          err.stack.split(/\n\s*/).slice(1).join('\n')
        );
      }
      var xmlBuilder = new xml2js.Builder();
      process.stdout.write(xmlBuilder.buildObject(data));
    }
  });
};

exports.input = function(fn) {
  var inputFile = process.argv[process.argv.length-1]
  var xml = {svg:{}}
  try {
    fn(inputFile, xml);
    var xmlBuilder = new xml2js.Builder();
    process.stdout.write(xmlBuilder.buildObject(xml));
  }
  catch (err) {
    console.error(
      err.message +'\n\n'+
      err.stack.split(/\n\s*/).slice(1).join('\n')
    );
  }
};
